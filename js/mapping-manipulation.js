const productsRow = document.querySelector(".sec2");


  function getProductCard({ name, category, image, price }) {
    let card = document.createElement("div");
    card.className = "card";

    // card body start
    let cardBody = document.createElement("div");
    cardBody.className = "card__body";

    let cardImg = document.createElement("img");
    cardImg.src = image;
    cardImg.alt = name;

    cardBody.appendChild(cardImg);

    // card footer start
    let cardFooter = document.createElement("div");
    cardFooter.className = "card__footer";

    let cardTitle = document.createElement("h1");
    let cardCategory = document.createElement("p");
    let cardPrice = document.createElement("p");

    let cardTitleText = document.createTextNode(name);
    let cardCategoryText = document.createTextNode(category);

    cardTitle.appendChild(cardTitleText);
    cardCategory.appendChild(cardCategoryText);
    cardPrice.innerHTML = `<b>Price: ${price}</b>`;

    cardFooter.append(cardTitle, cardCategory, cardPrice);

    card.append(cardBody, cardFooter);

    return card;
}

// for (let i = 1; i <= 8; i++) {
//   let card = getProductCard();
//   productsRow.append(card);
// }

products.slice(-4).forEach((el) => {
  let card = getProductCard(el);
  productsRow.append(card);
});

products
  .filter((el) => el.discount != 0)
  .slice(-4)
  .forEach((el) => {
    let card = getProductCard(el);
    sec2.append(card);
  });

